import csv
import io

from django.http.response import HttpResponse
from django.shortcuts import redirect, reverse

from rest_framework.generics import CreateAPIView
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView

from .models import Lender
from .serializers import LenderSerializer, CSVUploadSerializer


class LenderViewSet(ModelViewSet):
    queryset = Lender.objects.all()
    active_queryset = Lender.active_objects.all()
    serializer_class = LenderSerializer
    permission_classes = [IsAdminUser, ]

    def list(self, request, *args, **kwargs):
        active_filter = self.request.query_params.get("active")

        if active_filter:
            self.queryset = self.active_queryset

        return super().list(request, *args, **kwargs)



class LenderCSVExportAPIView(APIView):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="lenders_export.csv"'

        headers = [key for key in LenderSerializer().fields]

        writer = csv.DictWriter(response, fieldnames=headers)
        writer.writeheader()

        lenders = LenderSerializer(Lender.objects.all(), many=True)

        writer.writerows(lenders.data)

        return response


class LenderCSVImportAPIView(CreateAPIView):
    queryset = Lender.objects.all()
    serializer_class = CSVUploadSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        file = serializer.validated_data['file']

        decoded_file = file.read().decode()
        reader = csv.DictReader(io.StringIO(decoded_file))

        lenders = []

        for row in reader:
            serializer = LenderSerializer(data=row)
            if serializer.is_valid():
                lenders.append(serializer)
            else:
                raise ValidationError("There was a problem processing the file")

        for lender in lenders:
            # Only create the new instances if validation of all new objects is successful
            lender.save()

        return redirect(reverse("lender-list"))
