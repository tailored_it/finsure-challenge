from django.db import models


class ActiveLenderManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(active=True)


class Lender(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=3, unique=True)
    commission_rate = models.FloatField()
    trial_commission_rate = models.FloatField()
    active = models.BooleanField(default=True)

    objects = models.Manager()
    active_objects = ActiveLenderManager()


