"""
FinsureChallenge URL Configuration
"""
from django.contrib import admin
from django.urls import include, path

from rest_framework.routers import DefaultRouter

from lenders.api import LenderCSVExportAPIView, LenderCSVImportAPIView, LenderViewSet

router = DefaultRouter()
router.register(r'lenders', LenderViewSet, basename='lender')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/lenders/export', LenderCSVExportAPIView.as_view(), name='lenders_export'),
    path('api/lenders/import', LenderCSVImportAPIView.as_view(), name='lenders_import'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
