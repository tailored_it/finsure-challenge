# Finsure Challenge

Finsure Back-end Engineering Challenge

## Description

A simple RESTful API that allows administrators to manage lenders.

## Getting Started

### Dependencies

* Docker (https://docs.docker.com/get-docker/)
* docker-compose (https://docs.docker.com/compose/install/)

### Running the project

* Once docker and docker-compose are installed, this program can be brought up with the following command
```
docker-compose up -d
```
* When running the project for the first time you will need to migrate and create a superuser using the following commands
```bash
docker-compose run --rm web python manage.py migrate
```
```bash
docker-compose run --rm web python manage.py createsuperuser
```

## Author

Taylor Hughes-Scott